const { MessageEmbed } = require("discord.js");

module.exports.run = (client, message, args, con, lang) => {
    if (!args[1]) {
        let prefix = client.config.prefix
        let embed = new MessageEmbed()
            .setColor("#af0303")
            .setTitle("Menu d'aide")
            .setDescription("Vous pouvez avoir des informations sur une commande `"+prefix+"help <commande>`.")
        for (let i = 0; i < client.config.help.category.length; i++) {
            embed.addField("**" + client.config.help.category[i][0] + " ➢**", "- `" + client.commands.filter(com => com.help.category === client.config.help.category[i][1]).map(com => com.help.name).sort().join("`・`") + "`")
        }
        message.channel.send(embed).catch(err => err)
    } else {
            if (err) throw err;
            let prefix = client.config.prefix
            let cmd = args[1]
            let command;
            if (client.commands.has(cmd)) {
                command = client.commands.get(cmd);
            } else command = client.commands.get(client.alias.get(cmd));
            if (!command) return message.reply('Vous n\'avez pas donné une commande valide');
            let embed = new MessageEmbed()
            .setDescription(`**Commande:** ${command.help.name}`)
            .setColor("#af0303")
            .setTitle('Menu d\'aide')
            if (command.help.alias) {
                if (typeof(command.help.alias) === "string") embed.addField('Aliases:', `\`${command.help.alias}\``);
                else embed.addField('Aliases:', `\`${command.help.alias.join('`, `')}\``);
            }
            if (command.help.desc) embed.addField('Description:', `${command.help.desc}`);
            if (command.help.usage) embed.addField('Usage:',`\`${prefix}${command.help.name} ${command.help.usage}\``);
            else embed.addField('Usage:',`\`${prefix}${command.help.name}\``);
            let dmable = '<:Notcheck:693934215158300713>'
            if (command.help.dm) dmable = '<:Check:693934215300907019>'
            embed.addField('Utilisable en MP', dmable)
            message.channel.send(embed);
    }
}

module.exports.help = {
    name: "help",
    alias: "h",
    category: "info",
    desc: "Donne la liste des commandes du bot",
    dm: true,
    ownerOnly: false,
    modOnly: true,
    permRequired: [],
    botPerm: []
}